<?php

/**
 * @package ccPlatformSh
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    01 Sep 2017
 * */

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles( true );

$scriptSettings                   = array();
$scriptSettings['description']    = 'Overrides eZINI and eZSys PHP classes';
$scriptSettings['use-extensions'] = false;
$scriptSettings['use-session']    = false;
$scriptSettings['use-modules']    = false;

$script  = eZScript::instance($scriptSettings);
$script->startup();
// We are not calling initialize because we might not have correct cluster settings at this point
//$script->initialize();

$paths = array(
    'eZINI' => array(
        'orig' => 'lib/ezutils/classes/ezini.php',
        'dest' => 'extension/cc_platformsh/classes/ezini.php'
    ),
    'eZSys' => array(
        'orig' => 'lib/ezutils/classes/ezsys.php',
        'dest' => 'extension/cc_platformsh/classes/ezsys.php'
    )
);


$cli->output('Copying ' . $paths['eZINI']['orig'] . ' to ' . $paths['eZINI']['dest']);
if (copy($paths['eZINI']['orig'], $paths['eZINI']['dest']) === false) {
	$cli->output('Failed. Please check file permissions.');
	$script->shutdown(1);
}

$content        = file_get_contents($paths['eZINI']['dest']);
$changedPath    = $paths['eZINI']['dest'] . '.changed';
$changedContent = str_replace("<?php\n", "<?php\nnamespace custom;\n", $content);
file_put_contents($changedPath, $changedContent);
require $changedPath;
$reflection  = new ReflectionClass('\\custom\\eZINI');

$staticProps     = $reflection->getStaticProperties();
$useIniInjection = array_key_exists('injectedSettings', $staticProps);

if( $useIniInjection ) {
	$methodToModify = strpos($changedContent, 'function eZINI') !== false ? 'eZINI' : '__construct';
	$appendCode     = '
        $platformSettings       = ccPlatformShSettings::getInstance()->getSettings();
        foreach ($platformSettings as $file => $fileSettings) {
            foreach ($fileSettings as $block => $settings) {
                foreach ($settings as $setting => $value) {
                    self::$injectedSettings[$file][$block][$setting] = $value;
                }
            }
        }';
} else {
	$methodToModify = 'parse';
	$appendCode     = '
        $platformSettings = ccPlatformShSettings::getInstance()->getSettings();
        if (array_key_exists($this->FileName, $platformSettings))
        {
            foreach ($platformSettings[$this->FileName] as $block => $settings) {
                foreach ($settings as $setting => $value) {
                    $this->BlockValues[$block][$setting] = $value;
                }
            }
        }';

}
$cli->output('injectedSettings are in use: ' . ($useIniInjection ? 'yes' : 'no'));

$cli->output('Modifying content of eZINI file ...');

$method  = $reflection->getMethod($methodToModify);
$content = file($paths['eZINI']['orig']);
$endLine = $method->getEndLine() - 2;
$content[$endLine] = $appendCode . PHP_EOL . $content[$endLine];
$content = implode('', $content);
file_put_contents($paths['eZINI']['dest'], $content);
unlink($changedPath);


$cli->output('Copying ' . $paths['eZSys']['orig'] . ' to ' . $paths['eZSys']['dest']);
if (copy($paths['eZSys']['orig'], $paths['eZSys']['dest']) === false) {
    $cli->output('Failed. Please check file permissions.');
    $script->shutdown(1);
}

$findCode  = '    public static function hostname()
    {';
$extraCode = '
        // We need to set server port otherwise we got infinite recursion
        // self::hostname => self::serverProtocol => self::isSSLNow => self::serverPort => self::hostname
        if (empty($GLOBALS[\'eZSysServerPort\'])) {
            $port = (int) self::serverVariable(\'SERVER_PORT\', true);
            if ($port === 0) {
                $port = 80;
            }
            $GLOBALS[\'eZSysServerPort\'] = $port;
        }

        $isRedirect = false;
        $backtrace  = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        foreach ($backtrace as $call) {
            if (
                isset($call[\'class\']) && $call[\'class\'] === \'eZHTTPTool\' &&
                isset($call[\'function\']) && $call[\'function\'] === \'createRedirectUrl\'
            ) {
                $isRedirect = true;
            }
        }

        static $routes = null;
        if ($routes === null) {
            $routes = ccPlatformShSettings::getInstance()->getRoutes();
        }

        $key = self::serverProtocol() . \'://\' . self::serverVariable(\'HTTP_HOST\') . \'/\';
        if ($isRedirect === false && is_array($routes) && isset($routes[$key])) {
            $hostData = $routes[$key];
            if (isset($hostData[\'original_url\'])) {
                $originalHost = parse_url($hostData[\'original_url\'], PHP_URL_HOST);
                if ($originalHost && strpos($originalHost, \'{default}\') === false) {
                    return $originalHost;
                }
            }
        }
';

$cli->output('Modifying content of eZSys file ...');
$content = file_get_contents($paths['eZSys']['dest']);
$content = str_replace($findCode, $findCode . $extraCode, $content);

file_put_contents($paths['eZSys']['dest'], $content);

$cli->output('Done');
$script->shutdown(0);
